<?php
/**
 * This class handles all activities done in database for photo app
 *
*/
class Images extends CI_Model {
	/*
	 * This method get all images stored in the database.
	 */
	
	public function get_images($id='')
	{
		
		
		
		$this->db->select('ID,NAME, TAGS');
		
		if($id!='' || $id!=0)
		{
			$query = $this->db->get_where('photo_data', array('ID' => $id));
		}
		else 
		{
			$query = $this->db->get('photo_data');
		}
		
		
		
		if ($query->num_rows() > 0)
		{
			$row = $query;
		}		
		
		else 
		{
			$row='';
		}
	
		return $row;
	}
	
	/*
	 *This method handles the insert of data in the backend 
	 */
	public function insert_data($tablename,$data=array())
	{
		$this->db->insert($tablename, $data);
	}
    
	/*
	 * This method handles edit of data
	 */
	public function edit_data($tablename,$data,$datac)
	{
		$this->db->where('id', $datac);
		$this->db->update($tablename, $data);
		
	}
	
	/*
	 * this method gets all the tags that has been stored in the backend
	 */
	public function get_tags($tablename,$fields='')
	{
	
		
		$query = $this->db->get($tablename);
		
		return $query;
		
	}
}
