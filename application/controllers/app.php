<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller {

	public $tags = array();
	

	public function index()
	{
		$_POST= array();
		
		
		$this->load->view('view-upload',array('error'=>''));
	}
	public  function upload(){
		$this->load->model('images');
		$config['upload_path'] = 'uploaded/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
		
			$this->load->view('view-upload', $error);
		}
		else
		{
			$result = array('upload_data' => $this->upload->data());
			$data = array('NAME'=>$result['upload_data']['file_name'],'LOCATION'=>'uploaded/'.$result['upload_data']['file_name'],'TAGS'=>'','DATETIMEUPLOADED'=>date("Y-m-d G:i:s"));
			$this->images->insert_data('photo_data',$data);	
			redirect('/app/displayimage/'.$this->db->insert_id(), 'refresh');
		}
	}
	
	public function displayimage($id='')
	{
		$this->load->model('images');
		$tags= $this->images->get_tags('tags');
		$result['tags'] = $tags->result_array();
		
		$data = $this->images->get_images($id);
		if($data!='')
		{
		
		$result['data'] = $data->result_array();
		}
		
		$this->load->view('view-images',$result);
	}

	
	public function save()
	{
		$da='';
		$this->load->model('images');
		
		$data = $_POST;
		$jso= array(); 
		foreach ($data as $item => $value){
		 if($item!='ID')
		 {
		 	
		 	$da= $da.$value.' ';
		 	
		 }	
		}
		
		
		
		$this->images->edit_data('photo_data',array('TAGS'=>$da),$data['ID']);
		
		redirect('/app/displayimage/'.$data['ID'], 'refresh');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */